﻿using SharpGL;

namespace CurseProject.BussinessLogic.Interfaces
{
    public interface ICollisable
    {
        bool IsSolid { get; set; }
        bool IsMasksCollide(ICollisable obj);
        POINTFLOAT[] CollisionMaskPoints { get; set; }
    }
}
