﻿using SharpGL;

namespace CurseProject.BussinessLogic.Interfaces
{
    public interface IVisable
    {
        float[] ColorRGB3f { get; set; }

        string TexturePath { get; set; }

        void Draw(OpenGL context);
    }
}
