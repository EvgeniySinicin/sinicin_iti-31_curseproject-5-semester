﻿using SharpGL;

namespace CurseProject.BussinessLogic.Interfaces
{
    public interface IPrizeFactory
    {
        IPrize CreatePrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath);
    }
}
