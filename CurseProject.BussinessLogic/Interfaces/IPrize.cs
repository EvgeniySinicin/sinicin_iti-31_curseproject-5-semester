﻿using CurseProject.BussinessLogic.Models;

namespace CurseProject.BussinessLogic.Interfaces
{
    public interface IPrize
    {
        void Buff(Car car);
    }
}
