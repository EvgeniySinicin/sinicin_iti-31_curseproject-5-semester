﻿using CurseProject.BussinessLogic.Models;
using SharpGL;
using System.Collections.Generic;

namespace CurseProject.BussinessLogic.Interfaces
{
    public interface IMovable : ICollisable
    {
        float Speed { get; set; }

        void Move(GameField gameField, float offset);
        void Rotate(GameField gameField, float angle);
    }
}
