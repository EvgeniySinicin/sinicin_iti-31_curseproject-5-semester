﻿using SharpGL;

namespace CurseProject.BussinessLogic.Interfaces
{
    public interface IGameObject : ICollisable
    {
        POINTFLOAT[] Points { get; set; }
    }
}
