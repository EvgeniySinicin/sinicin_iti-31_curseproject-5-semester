﻿using CurseProject.BussinessLogic.Models.CarDecorators;
using SharpGL;

namespace CurseProject.BussinessLogic.Models
{
    public class FuelReplenishmentPrize : AbstractPrize
    {
        public FuelReplenishmentPrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath) : base(middlePoint, radius, positionNumber, texturePath)
        {
            base.ColorRGB3f = new float[] { 0f, 1f, 0f };
        }

        public override void Buff(Car car)
        {
            car = new FullTankCar(car);
        }
    }
}
