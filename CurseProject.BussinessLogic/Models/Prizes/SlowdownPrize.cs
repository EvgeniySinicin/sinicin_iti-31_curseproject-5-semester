﻿using CurseProject.BussinessLogic.Models.CarDecorators;
using SharpGL;

namespace CurseProject.BussinessLogic.Models
{
    public class SlowdownPrize : AbstractPrize
    {
        //public SlowdownPrize() : base(new POINTFLOAT(), 0, 13, texturePath) { }

        public SlowdownPrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath) : base(middlePoint, radius, positionNumber, texturePath)
        {
            ColorRGB3f = new float[] { 0.5f, 0f, 1f };
        }

        public override void Buff(Car car)
        {
            car = new SlowCar(car);
        }
    }
}
