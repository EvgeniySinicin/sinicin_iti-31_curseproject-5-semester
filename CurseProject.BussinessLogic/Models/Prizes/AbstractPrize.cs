﻿using CurseProject.BussinessLogic.Interfaces;
using SharpGL;
using SharpGL.SceneGraph.Assets;

namespace CurseProject.BussinessLogic.Models
{
    public abstract class AbstractPrize : AbstractGameObject, IVisable, IPrize
    {
        public AbstractPrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath)
            : base(new POINTFLOAT[] { middlePoint }, AbstractGameObject.CalculatePoints(middlePoint, radius * 2, radius * 2), false)
        {
            Radius = radius;
            PositionNumber = positionNumber;
            TexturePath = texturePath;
        }

        public Texture PrizeTexture { get; set; }

        public int PositionNumber { get; set; }

        public float Radius { get; set; }
        public float[] ColorRGB3f { get; set; }
        public string TexturePath { get; set; }

        public abstract void Buff(Car car);

        public virtual void Draw(OpenGL context)
        {
            #region Старая отрисовка
            // Отрисовка текстуры
            //// PrizeTexture.Create(context, @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\energy.png");
            //context.Color(ColorRGB3f[0], ColorRGB3f[1], ColorRGB3f[2], 0.5);
            //PrizeTexture.Bind(context);
            //context.Enable(OpenGL.GL_TEXTURE_2D);
            ////context.TexParameter(OpenGL.GL_TEXTURE_2D, OpenGL.GL_TEXTURE_WRAP_T, OpenGL.GL_CLAMP_TO_EDGE);

            //context.Begin(OpenGL.GL_QUADS);
            //context.TexCoord(0, 0); context.Vertex(CollisionMaskPoints[0].x, CollisionMaskPoints[0].y);
            //context.TexCoord(0, 1); context.Vertex(CollisionMaskPoints[1].x, CollisionMaskPoints[1].y);
            //context.TexCoord(1, 1); context.Vertex(CollisionMaskPoints[2].x, CollisionMaskPoints[2].y);
            //context.TexCoord(1, 0); context.Vertex(CollisionMaskPoints[3].x, CollisionMaskPoints[3].y);
            //context.End();
            //context.Disable(OpenGL.GL_TEXTURE_2D);

            // Отрисовка маски
            //var maskColor = new float[] { 0f, 1f, 1f };
            //context.Color(maskColor);
            //context.Begin(OpenGL.GL_QUADS);
            //foreach (var p in CollisionMaskPoints)
            //{
            //    context.Vertex(p.x, p.y, 0f);
            //}
            //context.End();

            // Отрисовка приза
            //context.Color(ColorRGB3f);
            //context.Begin(OpenGL.GL_TRIANGLE_FAN);
            //context.Vertex(Points[0].x, Points[0].y, 0.0f);
            //for (int angle = 0; angle <= 360; angle += 10)
            //{
            //    double x = Radius * Math.Cos(angle * Math.PI / 180);
            //    double y = Radius * Math.Sin(angle * Math.PI / 180);
            //    context.Vertex(x + Points[0].x, y + Points[0].y, 0.0f);
            //}
            //context.End();
            #endregion

            if (PrizeTexture == null)
            {
                PrizeTexture = new Texture();
                PrizeTexture.Create(context, TexturePath);
            }

            context.Color(1.0f, 1.0f, 1.0f, 0.0f);
            PrizeTexture.Bind(context);
            context.Enable(OpenGL.GL_TEXTURE_2D);

            context.Begin(OpenGL.GL_QUADS);
            context.TexCoord(0, 0); context.Vertex(CollisionMaskPoints[0].x, CollisionMaskPoints[0].y);
            context.TexCoord(0, 1); context.Vertex(CollisionMaskPoints[1].x, CollisionMaskPoints[1].y);
            context.TexCoord(1, 1); context.Vertex(CollisionMaskPoints[2].x, CollisionMaskPoints[2].y);
            context.TexCoord(1, 0); context.Vertex(CollisionMaskPoints[3].x, CollisionMaskPoints[3].y);
            context.End();
            context.Disable(OpenGL.GL_TEXTURE_2D);
        }
    }
}
