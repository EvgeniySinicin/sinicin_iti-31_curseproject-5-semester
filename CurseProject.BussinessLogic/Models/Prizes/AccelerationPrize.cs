﻿using CurseProject.BussinessLogic.Models.CarDecorators;
using SharpGL;

namespace CurseProject.BussinessLogic.Models
{
    public class AccelerationPrize : AbstractPrize
    {
        public AccelerationPrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath) : base(middlePoint, radius, positionNumber, texturePath)
        {
            base.ColorRGB3f = new float[] { 1f, 1f, 0f };
        }

        public override void Buff(Car car)
        {
            car = new FastCar(car);
        }
    }
}
