﻿namespace CurseProject.BussinessLogic.Models.CarDecorators
{
    public class FullTankCar : AbstractCarDecorator
    {
        public FullTankCar(Car car, int msecond = 5000) : base(car, msecond) { }

        public override void RemoveDecorations() { }

        public override void SetDecorations()
        {
            var additionalFuel = 25f;
            BaseCar.Fuel += additionalFuel;

            if (BaseCar.Fuel > Fuel)
            {
                BaseCar.Fuel = Fuel;
            }
        }
    }
}
