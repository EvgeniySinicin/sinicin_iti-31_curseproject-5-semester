﻿namespace CurseProject.BussinessLogic.Models.CarDecorators
{
    public class FastCar : AbstractCarDecorator
    {
        public FastCar(Car car, int msecond = 5000) : base(car, msecond) { }

        public override void RemoveDecorations() { }

        public override void SetDecorations()
        {
            var additionalNitro = 50f;
            var maxNitroPoints = 100f;
            BaseCar.Nitro += additionalNitro;

            if (BaseCar.Nitro > maxNitroPoints)
            {
                BaseCar.Nitro = maxNitroPoints;
            }

            BaseCar.HasNitro = true;
        }

        #region Старая реализация временного ускорения
        //public override void RemoveDecorations()
        //{
        //    BaseCar.Speed = Speed;
        //}

        //public override void SetDecorations()
        //{
        //    BaseCar.Speed *= 2f;
        //}
        #endregion
    }
}
