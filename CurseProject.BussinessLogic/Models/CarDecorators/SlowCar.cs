﻿namespace CurseProject.BussinessLogic.Models.CarDecorators
{
    public class SlowCar : AbstractCarDecorator
    {
        public SlowCar(Car car, int msecond = 5000) : base(car, msecond) { }

        public override void RemoveDecorations()
        {
            BaseCar.Speed = 1.0f;
            BaseCar.IsSlowMotionEffect = false;
        }

        public override void SetDecorations()
        {
            BaseCar.Speed = 0.5f;
            BaseCar.IsSlowMotionEffect = true;
        }

        #region Старая реализация замедления
        //public override void RemoveDecorations()
        //{
        //    BaseCar.Speed = Speed;
        //}

        //public override void SetDecorations()
        //{
        //    BaseCar.Speed /= 2f;
        //}
        #endregion
    }
}
