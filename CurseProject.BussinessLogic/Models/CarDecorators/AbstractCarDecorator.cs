﻿using System.Timers;

namespace CurseProject.BussinessLogic.Models.CarDecorators
{
    public abstract class AbstractCarDecorator : Car
    {
        protected Car BaseCar { get; set; }

        protected Timer timer;

        public AbstractCarDecorator(Car car, int msecond = 5000) 
            : base(car.Name, car.Points, car.ColorRGB3f, car.TexturePath)
        {
            BaseCar = car;

            SetDecorations();

            // Устанавливаем таймер
            timer = new Timer(msecond);
            timer.Elapsed += TimerCallBack;
            timer.AutoReset = false;
            timer.Enabled = true;
        }

        private void TimerCallBack(object obj, ElapsedEventArgs e)
        {
            timer.Enabled = false;
            timer.Dispose();
            RemoveDecorations();
        }

        public abstract void SetDecorations();
        public abstract void RemoveDecorations();
    }
}
