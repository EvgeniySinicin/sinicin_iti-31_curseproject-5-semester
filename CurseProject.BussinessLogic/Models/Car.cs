﻿using System;
using CurseProject.BussinessLogic.Interfaces;
using CurseProject.BussinessLogic.Models.CarDecorators;
using SharpGL;
using SharpGL.SceneGraph.Assets;

namespace CurseProject.BussinessLogic.Models
{
    public class Car : AbstractGameObject, IMovable, IVisable
    {
        public Texture CarTexture { get; set; }

        private readonly float height;
        private readonly float width;

        public string Name { get; set; }

        public POINTFLOAT MiddlePoint  = new POINTFLOAT { x = 0f, y = 0f };

        public float Speed { get; set; } = 1.0f;
        public float Fuel { get; set; } = 100.0f;
        public float Nitro { get; set; } = 0f;
        public float Angle { get; set;} = 90.0f;

        public bool IsSlowMotionEffect { get; set; } = false;
        public bool HasNitro { get; set; } = false;

        public int NextCheckpointNumber { get; set; } = 0;
        public int CountOfOvercameCircles { get; set; } = 0;

        public float[] ColorRGB3f { get; set; } = new float[3] { 1f, 1f, 0f };
        public string TexturePath { get; set; }

        public Car(string name, POINTFLOAT[] points, float[] colorRGB3f, string texturePath) 
            : base(points, points, true)
        {
            Name = name;
            MiddlePoint = CalculateMiddlePoint(points[0], points[2]);
            ColorRGB3f = colorRGB3f;
            height = points[1].y - points[0].y;
            width = points[3].x - points[0].x;
            TexturePath = texturePath;
        }

        // Отрисовка машинки
        public void Draw(OpenGL context)
        {
            if (CarTexture == null)
            {
                CarTexture = new Texture();
                CarTexture.Create(context, TexturePath);
            }
            
            
            CarTexture.Bind(context);
            context.Enable(OpenGL.GL_TEXTURE_2D);

            context.Color(ColorRGB3f[0], ColorRGB3f[1], ColorRGB3f[2], 1.0f);
            context.Begin(OpenGL.GL_QUADS);
            context.TexCoord(0, 0); context.Vertex(Points[0].x, Points[0].y);
            context.TexCoord(0, 1); context.Vertex(Points[1].x, Points[1].y);
            context.TexCoord(1, 1); context.Vertex(Points[2].x, Points[2].y);
            context.TexCoord(1, 0); context.Vertex(Points[3].x, Points[3].y);
            context.End();
            context.Disable(OpenGL.GL_TEXTURE_2D);
        }

        // Передвижение с проверкой на столкновение
        public void Move(GameField gameField, float offset)
        {
            var objs = gameField.GameObjects;

            if (Fuel > 0)
            {
                var isNotCollision = true;

                // Перемещаем машинку
                MoveWithoutCollisionCheck(Speed * offset);

                for (int i = 0; i < objs.Count; i++)
                {
                    // Проверяем столкнулась ли манишка с контрольной точкой
                    if (objs[i] is Checkpoint && this.IsMasksCollide(objs[i]))
                    {
                        var lastCheckpointNumber = NextCheckpointNumber;
                        NextCheckpointNumber = ((Checkpoint)objs[i]).Number;
                        if (lastCheckpointNumber == 3 && NextCheckpointNumber == 0)
                        {
                            CountOfOvercameCircles++;
                        }
                    }

                    // Проверяем столкнулся ли машинка с каким-либо объектом на карте за исключением себя
                    if (isNotCollision && !this.Equals(objs[i]))
                    {
                        // Проверяем столкнулась ли машинка со стенкой
                        if (objs[i] is ICollisable)
                        {
                            isNotCollision = !this.IsMasksCollide(objs[i]);

                            // Если машинка столкнулась и стенка твёрдая, то не даём ей проехать
                            if (!isNotCollision && objs[i].IsSolid)
                            {
                                // Вытесняем машинку из объекта при столкновении
                                MoveWithoutCollisionCheck(-offset);

                                // Циклически вытесняем машинку из препятствия
                                //while (this.IsMasksCollide(objs[i]))
                                //{
                                //    MoveWithoutCollisionCheck(-offset / 10f);
                                //}

                                new SlowCar(this).SetDecorations();
                            }
                        }

                        // Проверяем столкнулась ли машинка с призом
                        if (objs[i] is IPrize && this.IsMasksCollide(objs[i]))
                        {
                            // Накладываем эффект от приза
                            ((IPrize)objs[i]).Buff(this);

                            // Освобождаем позицию приза и удаляем его из игры
                            gameField.IsFreePrizePosition[((AbstractPrize)objs[i]).PositionNumber] = true;
                            objs.Remove(objs[i]);
                        }
                    }
                }

                // Если машинка сдвинулась и ни во что не врезалась, то сжигаем часть её топлива
                if (isNotCollision)
                {
                    Fuel -= Speed / Math.Abs(offset);
                }
            }
        }

        // Передвижение без проверки на столкновение
        public void MoveWithoutCollisionCheck(float offset)
        {
            for (int i = 0; i < Points.Length; i++)
            {
                Points[i].x += (float)(offset * Math.Cos(Angle * Math.PI / 180));
                Points[i].y += (float)(offset * Math.Sin(Angle * Math.PI / 180));
            }

            MiddlePoint.x += (float)(offset * Math.Cos(Angle * Math.PI / 180));
            MiddlePoint.y += (float)(offset * Math.Sin(Angle * Math.PI / 180));

            CollisionMaskPoints = Points;
        }

        // Поворот с проверкой на столкновение
        public void Rotate(GameField gameField, float angle)
        {
            var objs = gameField.GameObjects;

            var isNotCollision = true;

            // Поворачиваем машинку
            RotateWithoutCollisionCheck(angle);
            
            for (int i = 0; i < objs.Count; i++)
            {
                // Проверяем столкнулся ли машинка с каким-либо объектом на карте за исключением себя
                if (isNotCollision && !this.Equals(objs[i]))
                {
                    // Проверяем столкнулась ли машинка со стенкой
                    if (objs[i] is Wall)
                    {
                        isNotCollision = !this.IsMasksCollide(objs[i]);

                        // Если машинка столкнулась и стенка твёрдая, то не даём ей повернуться
                        if (!isNotCollision && objs[i].IsSolid)
                        {
                            // Вытесняем машинку из объекта при столкновении
                            // RotateWithoutCollisionCheck(-angle);

                            // Циклически вытесняем машинку из препятствия
                            while (this.IsMasksCollide(objs[i]))
                            {
                                RotateWithoutCollisionCheck(-angle / 10f);
                            }

                            new SlowCar(this).SetDecorations();
                        }
                    }

                    // Проверяем столкнулась ли машинка с призом
                    if (objs[i] is AbstractPrize && this.IsMasksCollide(objs[i]))
                    {
                        // Накладываем эффект от приза
                        ((IPrize)objs[i]).Buff(this);

                        // Освобождаем позицию приза и удаляем его из игры
                        gameField.IsFreePrizePosition[((AbstractPrize)objs[i]).PositionNumber] = true;
                        objs.Remove(objs[i]);
                    }
                }
            }
        }

        // Поворот без проверки на столкновение
        public void RotateWithoutCollisionCheck(float angle)
        {
            var halfCarHeight = height / 2;
            var halfCarWidth = width / 2;

            // Гипотенуза
            var dir = Math.Sqrt(halfCarHeight * halfCarHeight + halfCarWidth * halfCarWidth);
            var fi = Math.Atan(halfCarWidth / halfCarHeight) * 180 / Math.PI;

            Angle += angle;
            Angle = Angle % 360;

            // Нижняя левая точка
            Points[0].x = (float)(MiddlePoint.x + dir * Math.Cos(Math.PI * (180 + fi + Angle) / 180));
            Points[0].y = (float)(MiddlePoint.y + dir * Math.Sin(Math.PI * (180 + fi + Angle) / 180));

            // Верхняя левая точка
            Points[1].x = (float)(MiddlePoint.x + dir * Math.Cos(Math.PI * (180 - fi + Angle) / 180));
            Points[1].y = (float)(MiddlePoint.y + dir * Math.Sin(Math.PI * (180 - fi + Angle) / 180));

            // Верхняя правая точка
            Points[2].x = (float)(MiddlePoint.x + dir * Math.Cos(Math.PI * (Angle + fi) / 180));
            Points[2].y = (float)(MiddlePoint.y + dir * Math.Sin(Math.PI * (Angle + fi) / 180));

            // Нижняя правая точка
            Points[3].x = (float)(MiddlePoint.x + dir * Math.Cos(Math.PI * (-fi + Angle) / 180));
            Points[3].y = (float)(MiddlePoint.y + dir * Math.Sin(Math.PI * (-fi + Angle) / 180));

            CollisionMaskPoints = Points;
        }
    }
}
