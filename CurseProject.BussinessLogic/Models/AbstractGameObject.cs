﻿using CurseProject.BussinessLogic.Interfaces;
using System.Collections.Generic;
using SharpGL;

namespace CurseProject.BussinessLogic.Models
{
    public abstract class AbstractGameObject : IGameObject, ICollisable
    {
        public POINTFLOAT[] Points { get; set; }
        public POINTFLOAT[] CollisionMaskPoints { get; set; }

        public bool IsSolid { get; set; }

        public AbstractGameObject(POINTFLOAT[] points, POINTFLOAT[] collisionMaskPoints, bool isSolid)
        {
            Points = points;
            CollisionMaskPoints = collisionMaskPoints;
            IsSolid = isSolid;
        }
        
        public bool IsMasksCollide(ICollisable obj)
        {
            POINTFLOAT[] points1 = CollisionMaskPoints;
            POINTFLOAT[] points2 = obj.CollisionMaskPoints;

            // Стороны маски 1
            POINTFLOAT[] LBLT1 = new POINTFLOAT[] { points1[0], points1[1] };
            POINTFLOAT[] LTRT1 = new POINTFLOAT[] { points1[1], points1[2] };
            POINTFLOAT[] RTRB1 = new POINTFLOAT[] { points1[2], points1[3] };
            POINTFLOAT[] RBLB1 = new POINTFLOAT[] { points1[3], points1[0] };

            // Стороны маски 2
            POINTFLOAT[] LBLT2 = new POINTFLOAT[] { points2[0], points2[1] };
            POINTFLOAT[] LTRT2 = new POINTFLOAT[] { points2[1], points2[2] };
            POINTFLOAT[] RTRB2 = new POINTFLOAT[] { points2[2], points2[3] };
            POINTFLOAT[] RBLB2 = new POINTFLOAT[] { points2[3], points2[0] };

            List<POINTFLOAT[]> obj1sides = new List<POINTFLOAT[]> { LBLT1, LTRT1, RTRB1, RBLB1 };
            List<POINTFLOAT[]> obj2sides = new List<POINTFLOAT[]> { LBLT2, LTRT2, RTRB2, RBLB2 };

            var wasCollision = false;

            foreach (var side1 in obj1sides)
            {
                foreach (var side2 in obj2sides)
                {
                    if (!wasCollision)
                    {
                        wasCollision = IsSidesCollide(side2, side1);
                    }
                }
            }

            return wasCollision;
        }

        private bool IsSidesCollide(POINTFLOAT[] firstObjSide, POINTFLOAT[] secondObjSide)
        {
            POINTFLOAT p1 = firstObjSide[0];
            POINTFLOAT p2 = firstObjSide[1];

            POINTFLOAT p3 = secondObjSide[0];
            POINTFLOAT p4 = secondObjSide[1];

            //сначала расставим точки по порядку, т.е. чтобы было p1.x <= p2.x
            if (p2.x < p1.x)
            {
                POINTFLOAT tmp = p1;
                p1 = p2;
                p2 = tmp;
            }

            //и p3.x <= p4.x

            if (p4.x < p3.x)
            {
                POINTFLOAT tmp = p3;
                p3 = p4;
                p4 = tmp;
            }

            double Xa, b1, b2, A1, A2;

            //проверим существование потенциального интервала для точки пересечения отрезков
            if (p2.x < p3.x)
            {
                return false; //ибо у отрезков нету взаимной абсциссы
            }

            //если оба отрезка вертикальные
            if ((p1.x - p2.x == 0) && (p3.x - p4.x == 0))
            {
                //если они лежат на одном X
                if (p1.x == p3.x)
                {
                    //проверим пересекаются ли они, т.е. есть ли у них общий Y
                    //для этого возьмём отрицание от случая, когда они НЕ пересекаются
                    if (!((GetMax(p1.y, p2.y) < GetMin(p3.y, p4.y)) ||
                    (GetMin(p1.y, p2.y) > GetMax(p3.y, p4.y))))
                    {
                        return true;
                    }
                }
                return false;
            }

            //найдём коэффициенты уравнений, содержащих отрезки
            //f1(x) = A1*x + b1 = y
            //f2(x) = A2*x + b2 = y
            //если первый отрезок вертикальный
            if (p1.x - p2.x == 0)
            {
                //найдём Xa, Ya - точки пересечения двух прямых
                Xa = p1.x;
                A2 = (p3.y - p4.y) / (p3.x - p4.x);
                b2 = p3.y - A2 * p3.x;
                double Ya = A2 * Xa + b2;
                if (p3.x <= Xa && p4.x >= Xa && GetMin(p1.y, p2.y) <= Ya &&
                GetMax(p1.y, p2.y) >= Ya)
                {
                    return true;
                }
                return false;
            }

            //если второй отрезок вертикальный
            if (p3.x - p4.x == 0)
            {
                //найдём Xa, Ya - точки пересечения двух прямых
                Xa = p3.x;
                A1 = (p1.y - p2.y) / (p1.x - p2.x);
                b1 = p1.y - A1 * p1.x;
                double Ya = A1 * Xa + b1;
                if (p1.x <= Xa && p2.x >= Xa && GetMin(p3.y, p4.y) <= Ya &&
                GetMax(p3.y, p4.y) >= Ya)
                {
                    return true;
                }
                return false;
            }

            //оба отрезка невертикальные
            A1 = (p1.y - p2.y) / (p1.x - p2.x);
            A2 = (p3.y - p4.y) / (p3.x - p4.x);
            b1 = p1.y - A1 * p1.x;
            b2 = p3.y - A2 * p3.x;
            if (A1 == A2)
            {
                return false; //отрезки параллельны
            }

            //Xa - абсцисса точки пересечения двух прямых
            Xa = (b2 - b1) / (A1 - A2);
            if ((Xa < GetMax(p1.x, p3.x)) || (Xa > GetMin(p2.x, p4.x)))
            {
                return false; //точка Xa находится вне пересечения проекций отрезков на ось X
            }
            else
            {
                return true;
            }
        }

        public static POINTFLOAT CalculateMiddlePoint(POINTFLOAT leftBot, POINTFLOAT rightTop)
        {
            return new POINTFLOAT { x = (leftBot.x + rightTop.x) / 2, y = (leftBot.y + rightTop.y) / 2 };
        }

        public static POINTFLOAT[] CalculatePoints(POINTFLOAT middlePoint, float height, float width)
        {
            float halfCarHeight = height / 2f;
            float halfCarWidth = width / 2f;

            var leftBottom = new POINTFLOAT { x = middlePoint.x - halfCarWidth, y = middlePoint.y - halfCarHeight };
            var leftTop = new POINTFLOAT { x = middlePoint.x - halfCarWidth, y = middlePoint.y + halfCarHeight };
            var rightTop = new POINTFLOAT { x = middlePoint.x + halfCarWidth, y = middlePoint.y + halfCarHeight };
            var rightBottom = new POINTFLOAT { x = middlePoint.x + halfCarWidth, y = middlePoint.y - halfCarHeight };

            return new POINTFLOAT[] { leftBottom, leftTop, rightTop, rightBottom };
        }

        private float GetMax(float a, float b)
        {
            return a > b ? a : b;
        }

        private float GetMin(float a, float b)
        {
            return a < b ? a : b;
        }
    }
}
