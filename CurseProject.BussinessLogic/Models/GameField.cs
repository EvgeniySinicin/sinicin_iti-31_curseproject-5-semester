﻿using CurseProject.BussinessLogic.Interfaces;
using CurseProject.BussinessLogic.Models.PrizeFactories;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Timers;

namespace CurseProject.BussinessLogic.Models
{
    public class GameField : IVisable
    {
        public int CheckpointsCount { get; set; } = 0;
        public int CirclesCount { get; set; } = 5;
        public int Height { get; set; }
        public int Width { get; set; }

        public static float WallHeight { get; set; }
        public static float WallWidth { get; set; }

        public float[] ColorRGB3f { get; set; }

        public List<IGameObject> GameObjects = new List<IGameObject>();

        public Car Car1 { get; set; }
        public Car Car2 { get; set; }

        public string TexturePath { get; set; }

        // Инициализируем игровые объекты
        public GameField(int height, int width, OpenGL gl)
        {
            Height = height;
            Width = width;

            var carHeigth = 100f;
            var carWidth = 50f;
            var cellsCount = 10;

            WallHeight = Height / cellsCount;
            WallWidth = Width / cellsCount;

            #region Добавляем машинки
            var positionCar1 = AbstractGameObject.CalculatePoints(new POINTFLOAT
            {
                x = Width / 2 - WallWidth / 2 - WallWidth * 2,
                y = Height / 2 - WallHeight / 2 - WallHeight * 5
            }, carHeigth, carWidth);

            var positionCar2 = AbstractGameObject.CalculatePoints(new POINTFLOAT
            {
                x = Width / 2 - WallWidth / 2 - WallWidth * 1,
                y = Height / 2 - WallHeight / 2 - WallHeight * 5
            }, carHeigth, carWidth);

            Car1 = new Car("Зелёная машинка", positionCar1, new float[] { 1, 1, 0 }, @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\blueCar.png");
            Car2 = new Car("Синяя машинка", positionCar2, new float[] { 0, 0, 1 }, @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\blueCar.png");

            GameObjects.Add(Car1);
            GameObjects.Add(Car2);
            #endregion

            var wallColor = new float[] { 0.65f, 0.65f, 0.65f };
            var isWallSolid = true;

            #region Добавляем внешнее кольцо стен
            // Верхняя
            for (int i = 0; i < cellsCount; i++)
            {
                var positionWall = AbstractGameObject.CalculatePoints(new POINTFLOAT
                {
                    x = -(Width / 2) + (WallWidth / 2) + (i * WallWidth),
                    y = (Height / 2) - (WallHeight / 2)
                }, WallHeight, WallWidth);
                var wall = new Wall(positionWall, positionWall, isWallSolid, wallColor);
                GameObjects.Add(wall);
            }

            // Нижняя
            for (int i = 0; i < cellsCount; i++)
            {
                var positionWall = AbstractGameObject.CalculatePoints(new POINTFLOAT
                {
                    x = -(Width / 2) + (WallWidth / 2) + (i * WallWidth),
                    y = -(Height / 2) + (WallHeight / 2)
                }, WallHeight, WallWidth);
                var wall = new Wall(positionWall, positionWall, isWallSolid, wallColor);
                GameObjects.Add(wall);
            }

            // Левая
            for (int i = 1; i < cellsCount - 1; i++)
            {
                var positionWall = AbstractGameObject.CalculatePoints(new POINTFLOAT
                {
                    x = -(Width / 2) + (WallWidth / 2),
                    y = -(Height / 2) + (WallHeight / 2) + (i * WallHeight)
                }, WallHeight, WallWidth);
                var wall = new Wall(positionWall, positionWall, isWallSolid, wallColor);
                GameObjects.Add(wall);
            }

            // Правая
            for (int i = 1; i < cellsCount - 1; i++)
            {
                var positionWall = AbstractGameObject.CalculatePoints(new POINTFLOAT
                {
                    x = (Width / 2) - (WallWidth / 2),
                    y = (Height / 2) - (WallHeight / 2) - (i * WallHeight)
                }, WallHeight, WallWidth);
                var wall = new Wall(positionWall, positionWall, isWallSolid, wallColor);
                GameObjects.Add(wall);
            }
            #endregion

            #region Добавляем внутреннее кольцо стен
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    var positionWall = AbstractGameObject.CalculatePoints(new POINTFLOAT
                    {
                        x = -Width / 2 + WallWidth * 3 + WallWidth / 2 + i * WallWidth,
                        y = Height / 2 - WallHeight * 3 - WallHeight / 2 - j * WallHeight
                    }, WallHeight, WallWidth);
                    var wall = new Wall(positionWall, positionWall, isWallSolid, wallColor);
                    GameObjects.Add(wall);
                }
            }
            #endregion

            #region Добавляем призы
            // Создаём фабрики призов
            factories = new List<IPrizeFactory>()
            {
                new FuelReplenishmentPrizeFactory(),
                new AccelerationPrizeFactory(),
                new SlowdownPrizeFactory()
            };

            // Создаём позиции для призов
            prizePositions = new POINTFLOAT[]
        {
                // Правый верх карты
                new POINTFLOAT { x = WallWidth * 2 + WallWidth / 2, y = WallHeight * 2 + WallHeight / 2  },
                new POINTFLOAT { x = WallWidth * 3, y = WallHeight * 3 },

                // Верхняя середина карты
                new POINTFLOAT { x = WallWidth / 2, y = WallHeight * 2 + WallHeight / 2 },
                //new POINTFLOAT { x = 0, y = WallHeight * 3 },
                new POINTFLOAT { x = -WallWidth / 2, y = WallHeight * 3 + WallHeight / 2 },

                // Левый верх карты
                new POINTFLOAT { x = -WallWidth * 2 - WallWidth / 2, y = WallHeight * 2 + WallHeight / 2  },
                new POINTFLOAT { x = -WallWidth * 3, y = WallHeight * 3 },

                // Левая середина карты
                new POINTFLOAT { x = -WallWidth * 2 - WallWidth / 2, y = WallHeight  },
                new POINTFLOAT { x = -WallWidth * 3, y = -WallHeight  },
                new POINTFLOAT { x = -WallWidth * 3 - WallWidth / 2, y = WallHeight  },

                // Левый низ карты
                new POINTFLOAT { x = -WallWidth * 2 - WallWidth / 2, y = -WallHeight * 2 - WallHeight / 2  },
                new POINTFLOAT { x = -WallWidth * 3, y = -WallHeight * 3 },

                // Нижняя середина карты
                new POINTFLOAT { x = WallWidth / 2, y = -WallHeight * 2 - WallHeight / 2 },
                //new POINTFLOAT { x = 0, y = -WallHeight * 3 },
                new POINTFLOAT { x = -WallWidth / 2, y = -WallHeight * 3 - WallHeight / 2 },

                // Правый низ карты
                new POINTFLOAT { x = WallWidth * 2 + WallWidth / 2, y = -WallHeight * 2 - WallHeight / 2  },
                new POINTFLOAT { x = WallWidth * 3, y = -WallHeight * 3 }
        };

            IsFreePrizePosition = new bool[prizePositions.Length];
            
            for (int i = 0; i < IsFreePrizePosition.Length; i++)
            {
                IsFreePrizePosition[i] = true;
            }

            // Добавляем призы
            StartPrizeGeneration();
            #endregion

            #region Создаём чекпоинты
            var checkpointPositions = new POINTFLOAT[]
            {
                // Право
                new POINTFLOAT { x = WallWidth * 3, y = 0 },
                // Верх
                new POINTFLOAT { x = 0, y = WallHeight * 3 },
                // Лево
                new POINTFLOAT { x = -WallWidth * 3, y = 0 },
                // Низ
                new POINTFLOAT { x = 0, y = -WallHeight * 3 },
            };

            var checkpointSizes = new POINTFLOAT[]
            {
                // Право
                new POINTFLOAT { y = WallWidth / 10f, x = WallWidth * 2 },
                // Верх
                new POINTFLOAT { y = WallHeight * 2, x = WallWidth / 10f },
                // Лево
                new POINTFLOAT { y = WallWidth / 10f, x = WallWidth * 2 },
                // Низ
                new POINTFLOAT { y = WallHeight * 2, x = WallWidth / 10f }
            };

            for (int i = 0; i < checkpointPositions.Length; i++)
            {
                var isStart = false;

                if (i == 0)
                {
                    isStart = true;
                }

                var checkPoint = new Checkpoint(checkpointPositions[i], checkpointSizes[i].y, checkpointSizes[i].x, isStart);
                checkPoint.Number = i;
                GameObjects.Add(checkPoint);
                CheckpointsCount++;
            }
            #endregion
        }

        private Random random = new Random();
        private Timer timer;

        public void StartPrizeGeneration()
        {
            if (timer == null)
            {
                timer = new Timer(5000);
                timer.Elapsed += TimerCallBack;
                timer.AutoReset = true;
                timer.Enabled = true;
            }
        }

        private readonly List<IPrizeFactory> factories;

        private readonly POINTFLOAT[] prizePositions;

        public bool[] IsFreePrizePosition;

        // Обрабатываем тик таймера
        private void TimerCallBack(object obj, ElapsedEventArgs e)
        {
            var radius = 25f;

            // Проверяем, есть ли свободные позиции
            var isFreePositions = false;
            foreach (var p in IsFreePrizePosition)
            {
                if (!isFreePositions && p)
                {
                    isFreePositions = true;
                }
            }

            // Если есть свободная позиция, генерируем приз
            if (isFreePositions)
            {
                // Выбираем случайное число до тех пор, пока не выберем номер свободной позиции
                int positionNumber;
                string texturePath;

                do
                {
                    positionNumber = random.Next(0, prizePositions.Length);
                }
                while (!IsFreePrizePosition[positionNumber]);

                var position = prizePositions[positionNumber];
                IsFreePrizePosition[positionNumber] = false;
                var factoryNumber = random.Next(0, factories.Count);

                switch (factoryNumber)
                {
                    case 0:
                        {
                            texturePath = @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\fuel.png";
                            break;
                        }
                    case 1:
                        {
                            texturePath = @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\energy.png";
                            break;
                        }
                    case 2:
                        {
                            texturePath = @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\slow.png";
                            break;
                        }
                    default:
                        texturePath = string.Empty;
                        break;
                }

                var factory = factories[factoryNumber];
                var prize = factory.CreatePrize(position, radius, positionNumber, texturePath);
                //if (factoryNumber == 1)
                //{
                //    ((AbstractPrize)prize).PrizeTexture = new SharpGL.SceneGraph.Assets.Texture();
                //    ((AbstractPrize)prize).PrizeTexture.Create(Gl, @"E:\Учёба\КУРСОВАЯ\sinicin_iti-31_curseproject-5-semester\CurseProject.UI\Resources\energy.png");
                //}
                GameObjects.Add((AbstractGameObject)prize);
            }
        }

        public void StopPrizeGeneration()
        {
            if (timer != null)
            {
                timer.Enabled = false;
                timer.Dispose();
            }
        }

        public void Draw(OpenGL context)
        {
            for (int i = 0; i < GameObjects.Count; i++)
            {
                if (GameObjects[i] is IVisable)
                {
                    ((IVisable)GameObjects[i]).Draw(context);
                }
            }
        }
    }
}
