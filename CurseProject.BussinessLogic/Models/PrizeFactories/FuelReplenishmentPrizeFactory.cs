﻿using CurseProject.BussinessLogic.Interfaces;
using SharpGL;

namespace CurseProject.BussinessLogic.Models.PrizeFactories
{
    public class FuelReplenishmentPrizeFactory : IPrizeFactory
    {
        public IPrize CreatePrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath)
        {
            return new FuelReplenishmentPrize(middlePoint, radius, positionNumber, texturePath);
        }
    }
}
