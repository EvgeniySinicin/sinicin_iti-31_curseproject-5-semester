﻿using CurseProject.BussinessLogic.Interfaces;
using SharpGL;

namespace CurseProject.BussinessLogic.Models.PrizeFactories
{
    public class AccelerationPrizeFactory : IPrizeFactory
    {
        public IPrize CreatePrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath)
        {
            return new AccelerationPrize(middlePoint, radius, positionNumber, texturePath);
        }
    }
}
