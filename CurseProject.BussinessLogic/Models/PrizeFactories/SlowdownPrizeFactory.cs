﻿using CurseProject.BussinessLogic.Interfaces;
using SharpGL;

namespace CurseProject.BussinessLogic.Models.PrizeFactories
{
    public class SlowdownPrizeFactory : IPrizeFactory
    {
        public IPrize CreatePrize(POINTFLOAT middlePoint, float radius, int positionNumber, string texturePath)
        {
            return new SlowdownPrize(middlePoint, radius, positionNumber, texturePath);
        }
    }
}
