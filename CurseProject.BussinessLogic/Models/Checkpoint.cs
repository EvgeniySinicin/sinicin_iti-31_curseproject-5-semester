﻿using CurseProject.BussinessLogic.Interfaces;
using SharpGL;

namespace CurseProject.BussinessLogic.Models
{
    public class Checkpoint : AbstractGameObject, IVisable
    {
        public Checkpoint(POINTFLOAT middlePoint, float height, float width, bool isStart) 
            : base(CalculatePoints(middlePoint, height, width), CalculatePoints(middlePoint, height, width), false)
        {
            IsStart = isStart;
            Width = width;
            Height = height;
        }

        public int Number { get; set; } = 0;

        public float Width { get; set; }
        public float Height { get; set; }
        public float[] ColorRGB3f { get; set; } = new float[] { 0.75f, 0.75f, 0.75f };

        public string TexturePath { get; set; }

        public bool IsStart { get; set; }

        public void Draw(OpenGL context)
        {
            context.Color(ColorRGB3f);
            context.Begin(OpenGL.GL_QUADS);

            foreach (var p in Points)
            {
                context.Vertex(p.x, p.y, 0f);
            }

            // Помечаем стартовую контрольную точку
            if (IsStart)
            {
                var countOfMarks = 64;
                var markWidth = Width / countOfMarks;
                var markHeight = Height;

                for (int i = 0; i < countOfMarks; i++)
                {
                    if (i % 2 == 0)
                    {
                        var markPoints = CalculatePoints(new POINTFLOAT
                        {
                            x = Points[0].x + markWidth / 2 + markWidth * i,
                            y = Points[0].y + markHeight / 2
                        }, markHeight, markWidth);

                        context.Color(1.0f, 0.0f, 0.0f);

                        foreach (var p in markPoints)
                        {
                            context.Vertex(p.x, p.y, 0f);
                        }
                    }
                }
            }

            context.End();
        }
    }
}
