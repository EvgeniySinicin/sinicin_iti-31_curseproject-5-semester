﻿using CurseProject.BussinessLogic.Interfaces;
using SharpGL;
using SharpGL.SceneGraph.Assets;

namespace CurseProject.BussinessLogic.Models
{
    public class Wall : AbstractGameObject, IVisable
    {
        public Texture WallTexture { get; set; }

        public float[] ColorRGB3f { get; set; }
        public string TexturePath { get; set; }

        public Wall(POINTFLOAT[] points, POINTFLOAT[] collisionMaskPoints, bool isSolid, float[] color) 
            : base(points, collisionMaskPoints, isSolid)
        {
            ColorRGB3f = color;
        }

        public void Draw(OpenGL context)
        {
            
            context.Begin(OpenGL.GL_QUADS);

            var mid = CalculateMiddlePoint(Points[0], Points[2]);

            // Кирпич сверху справа
            
            //context.Vertex(mid.x, mid.y, 0f);
            //context.Vertex(mid.x, Points[2].y, 0f);
            //context.Vertex(Points[2].x, Points[2].y, 0f);
            //context.Vertex(Points[2].x, mid.y, 0f);

            //// Кирпич снизу слева
            //context.Vertex(Points[0].x, Points[0].y, 0f);
            //context.Vertex(Points[0].x, mid.y, 0f);
            //context.Vertex(mid.x, mid.y, 0f);
            //context.Vertex(mid.x, Points[0].y, 0f);

            var width = Points[2].x - Points[1].x;
            var height = Points[1].y - Points[0].y;
            var offset = 2.5f;
            var offset2 = 5;
            var additionalPoints = new POINTFLOAT[]
            {
                // Левый нижний кирпич
                new POINTFLOAT { x = Points[0].x + offset2, y = Points[0].y + offset2 },
                new POINTFLOAT { x = Points[0].x + offset2, y = Points[0].y + height/2 - offset },
                new POINTFLOAT { x = Points[0].x + width/2 - offset, y = Points[0].y + height/2 - offset },
                new POINTFLOAT { x = Points[0].x + width/2 - offset, y = Points[0].y +offset2 },

                // Левый верхний кирпич
                new POINTFLOAT { x = Points[1].x + offset2, y = Points[1].y - height/2 + offset },
                new POINTFLOAT { x = Points[1].x + offset2, y = Points[1].y - offset2 },
                new POINTFLOAT { x = Points[1].x + width/2 - offset, y = Points[1].y -offset2 },
                new POINTFLOAT { x = Points[1].x + width/2 - offset, y = Points[1].y - height/2 + offset },

                // Правый верхний кирпич
                new POINTFLOAT { x = Points[2].x - width/2 + offset, y = Points[2].y - height/2 + offset },
                new POINTFLOAT { x = Points[2].x - width/2 + offset, y = Points[2].y -offset2},
                new POINTFLOAT { x = Points[2].x-offset2, y = Points[2].y -offset2},
                new POINTFLOAT { x = Points[2].x -offset2, y = Points[2].y - height/2 + offset },

                // Правый нижний кирпич
                new POINTFLOAT { x = Points[3].x - width/2 + offset, y = Points[3].y +offset2},
                new POINTFLOAT { x = Points[3].x - width/2 + offset, y = Points[3].y + height/2 - offset },
                new POINTFLOAT { x = Points[3].x-offset2, y = Points[3].y + height/2 - offset },
                new POINTFLOAT { x = Points[3].x-offset2, y = Points[3].y+offset2},
            };
            //var additionalPoints = new POINTFLOAT[]
            //{
            //    // Левый нижний кирпич
            //    new POINTFLOAT { x = Points[0].x, y = Points[0].y },
            //    new POINTFLOAT { x = Points[0].x, y = Points[0].y + height/2 - offset },
            //    new POINTFLOAT { x = Points[0].x + width/2 - offset, y = Points[0].y + height/2 - offset },
            //    new POINTFLOAT { x = Points[0].x + width/2 - offset, y = Points[0].y },

            //    // Левый верхний кирпич
            //    new POINTFLOAT { x = Points[1].x, y = Points[1].y - height/2 + offset },
            //    new POINTFLOAT { x = Points[1].x, y = Points[1].y },
            //    new POINTFLOAT { x = Points[1].x + width/2 - offset, y = Points[1].y },
            //    new POINTFLOAT { x = Points[1].x + width/2 - offset, y = Points[1].y - height/2 + offset },

            //    // Правый верхний кирпич
            //    new POINTFLOAT { x = Points[2].x - width/2 + offset, y = Points[2].y - height/2 + offset },
            //    new POINTFLOAT { x = Points[2].x - width/2 + offset, y = Points[2].y },
            //    new POINTFLOAT { x = Points[2].x, y = Points[2].y },
            //    new POINTFLOAT { x = Points[2].x, y = Points[2].y - height/2 + offset },

            //    // Правый нижний кирпич
            //    new POINTFLOAT { x = Points[3].x - width/2 + offset, y = Points[3].y },
            //    new POINTFLOAT { x = Points[3].x - width/2 + offset, y = Points[3].y + height/2 - offset },
            //    new POINTFLOAT { x = Points[3].x, y = Points[3].y + height/2 - offset },
            //    new POINTFLOAT { x = Points[3].x, y = Points[3].y},
            //};

            context.Color(ColorRGB3f);
            foreach (var p in Points)
            {
                context.Vertex(p.x, p.y, 0f);
            }

            context.Color(0.937f, 0.38f, 0.28f);
            foreach (var p in additionalPoints)
            {
                context.Vertex(p.x, p.y);
            }

            

            context.End();
        }
    }
}
