﻿using System;
using System.Windows.Forms;
using System.Windows.Input;
using CurseProject.BussinessLogic.Models;
using SharpGL;
using SharpGL.SceneGraph.Assets;

namespace CurseProject.UI
{
    public partial class MainForm : Form
    {
        public GameField GF { get; set; }

        public static OpenGL gl = null;

        public MainForm()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLDraw(object sender, RenderEventArgs args)
        {
            var progressBarCoeff = FirstCarWayBar.Maximum / GF.CirclesCount;

            FirstCarFuelBar.Value = (int)GF.Car1.Fuel;
            FirstCarNitroBar.Value = (int)GF.Car1.Nitro;
            FirstCarWayBar.Value = GF.Car1.CountOfOvercameCircles * progressBarCoeff;

            SecondCarFuelBar.Value = (int)GF.Car2.Fuel;
            SecondCarNitroBar.Value = (int)GF.Car2.Nitro;
            SecondCarWayBar.Value = GF.Car2.CountOfOvercameCircles * progressBarCoeff;

            // Вывод координат машинок на экран
            FirstCarParamsLabel.Text = $"Lada 2109\n" +
                                $"Скорость: {GF.Car1.Speed}\n" +
                                $"Топливо: \n\n" +
                                $"Нитро: \n\n" +
                                $"Преодолено: ";

            SecondCarParamsLabel.Text = $"Tesla Cybertruck\n" +
                                $"Скорость: {GF.Car2.Speed}\n" +
                                $"Энергия: \n\n" +
                                $"Нитро: \n\n" +
                                $"Преодолено: ";


            WinParams.Text = $"WinParams:\n" +
                            $"Height:{GF.Height};\n" +
                            $"Width:{GF.Width}\n" +
                            $"H/10:{Height / 10}\n" +
                            $"W/10:{Width / 10}\n";

            // Проверяем не закончилась ли игра
            EndGameCheck();

            // Очищаем фрагментный и глубинный буфер
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

            gl.MatrixMode(OpenGL.GL_PROJECTION);
            gl.LoadIdentity();
            gl.Ortho2D(-GF.Width / 2, GF.Width / 2, -GF.Height / 2, GF.Height / 2);
            gl.MatrixMode(OpenGL.GL_MODELVIEW);
            gl.LoadIdentity();

            KeyboardHandler(GF.Car1, Key.W, Key.S, Key.D, Key.A, Key.E, Key.Q, Key.LeftShift);
            KeyboardHandler(GF.Car2, Key.Up, Key.Down, Key.Right, Key.Left, Key.NumPad1, Key.RightShift, Key.Enter);

            // Отрисовываем все видимые игровые объекты
            GF.Draw(gl);

            gl.Flush();
        }

        private void EndGameCheck()
        {
            // Проверяем топливо в машинках
            if (GF.Car1.Fuel <= 0)
            {
                BlueWinLabel.Visible = true;
                GF.Car2.Speed = 0;
                GF.StopPrizeGeneration();
            }

            if (GF.Car2.Fuel <= 0)
            {
                GreenWinLabel.Visible = true;
                GF.Car1.Speed = 0;
                GF.StopPrizeGeneration();
            }
            
            if (GF.Car1.Fuel <= 0 && GF.Car2.Fuel <= 0)
            {
                var countCar1CheckPoints = GF.Car1.CountOfOvercameCircles * GF.CheckpointsCount + GF.Car1.NextCheckpointNumber;
                var countCar2CheckPoints = GF.Car2.CountOfOvercameCircles * GF.CheckpointsCount + GF.Car2.NextCheckpointNumber;

                if (countCar1CheckPoints > countCar2CheckPoints)
                {
                    GreenWinLabel.Visible = true;
                    GF.Car2.Speed = 0;
                }
                else if (countCar1CheckPoints < countCar2CheckPoints)
                {
                    BlueWinLabel.Visible = true;
                    GF.Car1.Speed = 0;
                }
                else
                {
                    NobodyWinLabel.Visible = true;
                }

                GF.StopPrizeGeneration();
            }

            // Проверяем проехала ли одна из машин 5 кругов.
            foreach (var c in new Car[] { GF.Car1, GF.Car2 })
            {
                if (c.CountOfOvercameCircles == GF.CirclesCount)
                {
                    if (c.Equals(GF.Car1))
                    {
                        GreenWinLabel.Visible = true;
                    }
                    else
                    {
                        BlueWinLabel.Visible = true;
                    }
                    GF.Car1.Speed = 0;
                    GF.Car2.Speed = 0;
                    GF.StopPrizeGeneration();
                }
            }
        }

        // Настраиваем управление с клавиатуры
        private void KeyboardHandler(Car car, Key Up, Key Down, Key Right, Key Left, Key Clockwise, Key Сounterclockwise, Key Booster)
        {
            const float offset = 10f;
            const float angle = 10f;

            if (Keyboard.IsKeyDown(Right) && Keyboard.IsKeyDown(Up)) // ВПРАВО ВВЕРХ
            {
                car.Move(GF, offset);
                car.Rotate(GF, -angle);
            }
            else if (Keyboard.IsKeyDown(Right) && Keyboard.IsKeyDown(Down)) // ВПРАВО ВНИЗ
            {
                car.Move(GF, -offset);
                car.Rotate(GF, angle);
            }
            else if (Keyboard.IsKeyDown(Left) && Keyboard.IsKeyDown(Up)) // ВЛЕВО ВВЕРХ
            {
                car.Move(GF, offset);
                car.Rotate(GF, angle);
            }
            else if (Keyboard.IsKeyDown(Left) && Keyboard.IsKeyDown(Down)) // ВЛЕВО ВНИЗ
            {
                car.Move(GF, -offset);
                car.Rotate(GF, -angle);
            }
            else if (Keyboard.IsKeyDown(Up)) // ВВЕРХ
            {
                car.Move(GF, offset);
            }
            else if (Keyboard.IsKeyDown(Down)) // ВНИЗ
            {
                car.Move(GF, -offset);
            }
            else if (Keyboard.IsKeyDown(Right)) // ВПРАВО
            {
                car.Rotate(GF, -angle);
            }
            else if (Keyboard.IsKeyDown(Left)) // ВЛЕВО
            {
                car.Rotate(GF, angle);
            }

            if (Keyboard.IsKeyDown(Booster) && !car.IsSlowMotionEffect && car.HasNitro)
            {
                car.Speed = 2.0f;
                car.Nitro--;

                // Если у машины нет нитро, но она по каким-то мистическим силам едет со скоростью > 1
                if (car.Nitro <= 0 && car.Speed > 1.0f)
                {
                    car.Speed = 1.0f;
                    car.HasNitro = false;
                }
            }
            if(Keyboard.IsKeyUp(Booster) && !car.IsSlowMotionEffect)
            {
                car.Speed = 1.0f;
            }
        }

        // Инициализация параметров камеры
        private void OpenGLControl_OpenGLInitialized(object sender, EventArgs e)
        {
            //// Получаем экземпляр окна отрисовки
            gl = OpenGLControl.OpenGL;

            GF = new GameField(OpenGLControl.Height, OpenGLControl.Width, gl);

            GF.Car1.Rotate(GF, 0);
            GF.Car2.Rotate(GF, 0);

            //Цвет дороги
            //gl.ClearColor(0.8f, 0.52f, 0.247f, 0.0f);
            gl.ClearColor(0.0f, 0.0f, 0.0f, 0.0f);

            var wallTexture = new Texture();
        }

        // Изменяем размер игрового поля при изменении размера окна
        private void OpenGLControl_Resized(object sender, EventArgs e)
        {
            GF.Height = OpenGLControl.Height;
            GF.Width = OpenGLControl.Width;
        }
    }
}
