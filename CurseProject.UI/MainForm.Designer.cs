﻿namespace CurseProject.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OpenGLControl = new SharpGL.OpenGLControl();
            this.FirstCarParamsLabel = new System.Windows.Forms.Label();
            this.WinParams = new System.Windows.Forms.Label();
            this.SecondCarParamsLabel = new System.Windows.Forms.Label();
            this.GreenWinLabel = new System.Windows.Forms.Label();
            this.FirstCarFuelBar = new System.Windows.Forms.ProgressBar();
            this.FirstCarWayBar = new System.Windows.Forms.ProgressBar();
            this.SecondCarFuelBar = new System.Windows.Forms.ProgressBar();
            this.SecondCarWayBar = new System.Windows.Forms.ProgressBar();
            this.FirstCarNitroBar = new System.Windows.Forms.ProgressBar();
            this.SecondCarNitroBar = new System.Windows.Forms.ProgressBar();
            this.BlueWinLabel = new System.Windows.Forms.Label();
            this.NobodyWinLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.OpenGLControl)).BeginInit();
            this.SuspendLayout();
            // 
            // OpenGLControl
            // 
            this.OpenGLControl.AutoSize = true;
            this.OpenGLControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OpenGLControl.DrawFPS = true;
            this.OpenGLControl.FrameRate = 30;
            this.OpenGLControl.Location = new System.Drawing.Point(0, 0);
            this.OpenGLControl.Name = "OpenGLControl";
            this.OpenGLControl.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_1;
            this.OpenGLControl.RenderContextType = SharpGL.RenderContextType.DIBSection;
            this.OpenGLControl.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.OpenGLControl.Size = new System.Drawing.Size(1920, 1080);
            this.OpenGLControl.TabIndex = 0;
            this.OpenGLControl.OpenGLInitialized += new System.EventHandler(this.OpenGLControl_OpenGLInitialized);
            this.OpenGLControl.OpenGLDraw += new SharpGL.RenderEventHandler(this.OpenGLControl_OpenGLDraw);
            this.OpenGLControl.Resized += new System.EventHandler(this.OpenGLControl_Resized);
            // 
            // FirstCarParamsLabel
            // 
            this.FirstCarParamsLabel.AutoSize = true;
            this.FirstCarParamsLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.FirstCarParamsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FirstCarParamsLabel.ForeColor = System.Drawing.Color.ForestGreen;
            this.FirstCarParamsLabel.Location = new System.Drawing.Point(0, 0);
            this.FirstCarParamsLabel.Name = "FirstCarParamsLabel";
            this.FirstCarParamsLabel.Padding = new System.Windows.Forms.Padding(23, 0, 23, 45);
            this.FirstCarParamsLabel.Size = new System.Drawing.Size(192, 237);
            this.FirstCarParamsLabel.TabIndex = 1;
            this.FirstCarParamsLabel.Text = "Батин жигуль\r\nСкорость: \r\nТопливо:\r\n\r\nНитро:\r\n\r\nПреодолено: \r\n.";
            // 
            // WinParams
            // 
            this.WinParams.AutoSize = true;
            this.WinParams.Location = new System.Drawing.Point(255, 9);
            this.WinParams.Name = "WinParams";
            this.WinParams.Size = new System.Drawing.Size(64, 13);
            this.WinParams.TabIndex = 3;
            this.WinParams.Text = "WinParams:";
            this.WinParams.Visible = false;
            // 
            // SecondCarParamsLabel
            // 
            this.SecondCarParamsLabel.AutoSize = true;
            this.SecondCarParamsLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.SecondCarParamsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SecondCarParamsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.SecondCarParamsLabel.Location = new System.Drawing.Point(1729, 0);
            this.SecondCarParamsLabel.Name = "SecondCarParamsLabel";
            this.SecondCarParamsLabel.Padding = new System.Windows.Forms.Padding(24, 0, 24, 45);
            this.SecondCarParamsLabel.Size = new System.Drawing.Size(192, 237);
            this.SecondCarParamsLabel.TabIndex = 4;
            this.SecondCarParamsLabel.Text = "Тэсла\r\nСкорость: \r\nТопливо:\r\n\r\nНитро:\r\n\r\nПреодолено: \r\n.";
            // 
            // GreenWinLabel
            // 
            this.GreenWinLabel.AutoSize = true;
            this.GreenWinLabel.BackColor = System.Drawing.Color.Black;
            this.GreenWinLabel.Font = new System.Drawing.Font("Times New Roman", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreenWinLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.GreenWinLabel.Location = new System.Drawing.Point(647, 500);
            this.GreenWinLabel.Name = "GreenWinLabel";
            this.GreenWinLabel.Size = new System.Drawing.Size(633, 73);
            this.GreenWinLabel.TabIndex = 5;
            this.GreenWinLabel.Text = "Победила Lada 2109!";
            this.GreenWinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.GreenWinLabel.Visible = false;
            // 
            // FirstCarFuelBar
            // 
            this.FirstCarFuelBar.ForeColor = System.Drawing.Color.ForestGreen;
            this.FirstCarFuelBar.Location = new System.Drawing.Point(30, 70);
            this.FirstCarFuelBar.Name = "FirstCarFuelBar";
            this.FirstCarFuelBar.Size = new System.Drawing.Size(146, 20);
            this.FirstCarFuelBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.FirstCarFuelBar.TabIndex = 6;
            // 
            // FirstCarWayBar
            // 
            this.FirstCarWayBar.ForeColor = System.Drawing.Color.Gold;
            this.FirstCarWayBar.Location = new System.Drawing.Point(30, 168);
            this.FirstCarWayBar.Name = "FirstCarWayBar";
            this.FirstCarWayBar.Size = new System.Drawing.Size(146, 20);
            this.FirstCarWayBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.FirstCarWayBar.TabIndex = 7;
            // 
            // SecondCarFuelBar
            // 
            this.SecondCarFuelBar.ForeColor = System.Drawing.Color.RoyalBlue;
            this.SecondCarFuelBar.Location = new System.Drawing.Point(1760, 70);
            this.SecondCarFuelBar.Name = "SecondCarFuelBar";
            this.SecondCarFuelBar.Size = new System.Drawing.Size(144, 20);
            this.SecondCarFuelBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.SecondCarFuelBar.TabIndex = 8;
            // 
            // SecondCarWayBar
            // 
            this.SecondCarWayBar.ForeColor = System.Drawing.Color.Gold;
            this.SecondCarWayBar.Location = new System.Drawing.Point(1760, 168);
            this.SecondCarWayBar.Name = "SecondCarWayBar";
            this.SecondCarWayBar.Size = new System.Drawing.Size(144, 20);
            this.SecondCarWayBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.SecondCarWayBar.TabIndex = 9;
            // 
            // FirstCarNitroBar
            // 
            this.FirstCarNitroBar.ForeColor = System.Drawing.Color.Crimson;
            this.FirstCarNitroBar.Location = new System.Drawing.Point(30, 118);
            this.FirstCarNitroBar.Name = "FirstCarNitroBar";
            this.FirstCarNitroBar.Size = new System.Drawing.Size(146, 20);
            this.FirstCarNitroBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.FirstCarNitroBar.TabIndex = 10;
            // 
            // SecondCarNitroBar
            // 
            this.SecondCarNitroBar.ForeColor = System.Drawing.Color.Crimson;
            this.SecondCarNitroBar.Location = new System.Drawing.Point(1760, 118);
            this.SecondCarNitroBar.Name = "SecondCarNitroBar";
            this.SecondCarNitroBar.Size = new System.Drawing.Size(144, 20);
            this.SecondCarNitroBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.SecondCarNitroBar.TabIndex = 11;
            // 
            // BlueWinLabel
            // 
            this.BlueWinLabel.AutoSize = true;
            this.BlueWinLabel.BackColor = System.Drawing.Color.Black;
            this.BlueWinLabel.Font = new System.Drawing.Font("Times New Roman", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BlueWinLabel.ForeColor = System.Drawing.Color.Blue;
            this.BlueWinLabel.Location = new System.Drawing.Point(704, 464);
            this.BlueWinLabel.Name = "BlueWinLabel";
            this.BlueWinLabel.Size = new System.Drawing.Size(481, 146);
            this.BlueWinLabel.TabIndex = 12;
            this.BlueWinLabel.Text = "Победила Tesla \r\nCybertruck!";
            this.BlueWinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BlueWinLabel.Visible = false;
            // 
            // NobodyWinLabel
            // 
            this.NobodyWinLabel.AutoSize = true;
            this.NobodyWinLabel.BackColor = System.Drawing.Color.Black;
            this.NobodyWinLabel.Font = new System.Drawing.Font("Times New Roman", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NobodyWinLabel.ForeColor = System.Drawing.Color.Red;
            this.NobodyWinLabel.Location = new System.Drawing.Point(684, 480);
            this.NobodyWinLabel.Name = "NobodyWinLabel";
            this.NobodyWinLabel.Padding = new System.Windows.Forms.Padding(0, 20, 0, 20);
            this.NobodyWinLabel.Size = new System.Drawing.Size(553, 113);
            this.NobodyWinLabel.TabIndex = 13;
            this.NobodyWinLabel.Text = "Победила дружба!";
            this.NobodyWinLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.NobodyWinLabel.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.NobodyWinLabel);
            this.Controls.Add(this.BlueWinLabel);
            this.Controls.Add(this.SecondCarNitroBar);
            this.Controls.Add(this.FirstCarNitroBar);
            this.Controls.Add(this.SecondCarWayBar);
            this.Controls.Add(this.SecondCarFuelBar);
            this.Controls.Add(this.FirstCarWayBar);
            this.Controls.Add(this.FirstCarFuelBar);
            this.Controls.Add(this.GreenWinLabel);
            this.Controls.Add(this.SecondCarParamsLabel);
            this.Controls.Add(this.WinParams);
            this.Controls.Add(this.FirstCarParamsLabel);
            this.Controls.Add(this.OpenGLControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.OpenGLControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private SharpGL.OpenGLControl OpenGLControl;
        private System.Windows.Forms.Label FirstCarParamsLabel;
        private System.Windows.Forms.Label WinParams;
        private System.Windows.Forms.Label SecondCarParamsLabel;
        private System.Windows.Forms.Label GreenWinLabel;
        private System.Windows.Forms.ProgressBar FirstCarFuelBar;
        private System.Windows.Forms.ProgressBar FirstCarWayBar;
        private System.Windows.Forms.ProgressBar SecondCarFuelBar;
        private System.Windows.Forms.ProgressBar SecondCarWayBar;
        private System.Windows.Forms.ProgressBar FirstCarNitroBar;
        private System.Windows.Forms.ProgressBar SecondCarNitroBar;
        private System.Windows.Forms.Label BlueWinLabel;
        private System.Windows.Forms.Label NobodyWinLabel;
    }
}