﻿using CurseProject.BussinessLogic.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpGL;

namespace CurseProject.UnitTesting.BusinessLogicTests.AbstractGameObjectTests
{
    [TestClass]
    public class AbstractGameObjectTest
    {
        [TestMethod]
        public void CollisionChecking()
        {
            var wallPosition = new POINTFLOAT[]
                {
                    new POINTFLOAT { x = -1.0f, y = 0.0f },
                    new POINTFLOAT { x = -1.0f, y = 1.0f },
                    new POINTFLOAT { x = 1.0f, y = 1.0f },
                    new POINTFLOAT { x = 1.0f, y = 0.0f },
                };

            var wallColor = new float[] { 0.0f, 0.0f, 0.0f };
            var wall = new Wall(wallPosition, wallPosition, true, wallColor);

            var carPosition = new POINTFLOAT[]
                {
                    new POINTFLOAT { x = -0.5f, y = -0.5f },
                    new POINTFLOAT { x = -0.5f, y = 0.5f },
                    new POINTFLOAT { x = 0.5f, y = 0.5f },
                    new POINTFLOAT { x = 0.5f, y = -0.5f },
                };

            var carColor = wallColor;
            var carName = "TestCarName";
            var car = new Car(carName, carPosition, carColor, null);

            var expected = true;
            var actual = car.IsMasksCollide(wall);

            Assert.AreEqual(expected, actual);
        }
    }
}
