﻿using CurseProject.BussinessLogic.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpGL;

namespace CurseProject.CarTests.UnitTesting.BusinessLogicTests
{
    [TestClass]
    public class CarTest
    {
        private Car car;

        [TestMethod]
        public void CarInitialization()
        {
            var carName = "TestName";

            var startCarPosition = new POINTFLOAT[]
            {
                new POINTFLOAT { x = -0.5f, y = -0.5f  },
                new POINTFLOAT { x = -0.5f, y = 0.5f  },
                new POINTFLOAT { x = 0.5f, y = 0.5f  },
                new POINTFLOAT { x = 0.5f, y = -0.5f  },
            };

            var carColor = new float[] { 0.0f, 0.0f, 0.0f };

            car = new Car(carName, startCarPosition, carColor, null);

            Assert.AreEqual(carName, car.Name);
            Assert.AreEqual(startCarPosition, car.Points);
            Assert.AreEqual(carColor, car.ColorRGB3f);
        }

        [TestMethod]
        public void MoveUpOnTenPixels()
        {
            CarInitialization();

            var offset = 10;

            var expectedCarPosition = new POINTFLOAT[]
            {
                new POINTFLOAT { x = -0.5f, y = -0.5f + offset },
                new POINTFLOAT { x = -0.5f, y = 0.5f + offset },
                new POINTFLOAT { x = 0.5f, y = 0.5f + offset },
                new POINTFLOAT { x = 0.5f, y = -0.5f + offset },
            };

            car.MoveWithoutCollisionCheck(offset);

            var actualCarPosition = car.Points;

            Assert.AreEqual(expectedCarPosition[0].y, actualCarPosition[0].y);
            Assert.AreEqual(expectedCarPosition[0].x, actualCarPosition[0].x);
            Assert.AreEqual(expectedCarPosition[1].y, actualCarPosition[1].y);
            Assert.AreEqual(expectedCarPosition[1].x, actualCarPosition[1].x);
            Assert.AreEqual(expectedCarPosition[2].y, actualCarPosition[2].y);
            Assert.AreEqual(expectedCarPosition[2].x, actualCarPosition[2].x);
            Assert.AreEqual(expectedCarPosition[3].y, actualCarPosition[3].y);
            Assert.AreEqual(expectedCarPosition[3].x, actualCarPosition[3].x);
        }

        [TestMethod]
        public void CarMiddlePointCheck()
        {
            CarInitialization();

            var expectedMiddlePoint = new POINTFLOAT { x = 0, y = 0 };
            var actualMiddlePoint = car.MiddlePoint;

            Assert.AreEqual(expectedMiddlePoint, actualMiddlePoint);
        }

        [TestMethod]
        public void RotationOn90Degrees()
        {
            CarInitialization();

            var rotateAngle = 90;
            var currentCarAngle = car.Angle;
            var expectedCarAngle = currentCarAngle + rotateAngle;

            car.RotateWithoutCollisionCheck(rotateAngle);

            Assert.AreEqual(expectedCarAngle, car.Angle);
        }
    }
}
